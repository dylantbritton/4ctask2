package com.fcs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class main {

	public static void main(String[] args) {

		String file = args[0];
		String order = args[1];

		boolean asc = false;
		boolean valid = true;
		if (order.equals("asc")) {
			asc = true;
		} else if (order.equals("desc") || order == null) {
			asc = false;
		} else {
			System.out.println("Invalid entry");
			valid = false;
		}
		if (valid) {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				List<Long> numbers = new ArrayList<>();
				while (line != null) {
					try {
						numbers.add(Long.parseLong(line));
					} catch (Exception e) {

					}
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				for (int i = 0; i < numbers.size(); i++) {
					for (int j = i + 1; j < numbers.size(); j++) {
						if (asc && numbers.get(i) > numbers.get(j)) {
							Long temp = numbers.get(i);
							numbers.set(i, numbers.get(j));
							numbers.set(j, temp);
						}
						if (!asc && numbers.get(i) < numbers.get(j)) {
							Long temp = numbers.get(j);
							numbers.set(j, numbers.get(i));
							numbers.set(i, temp);
						}
					}
				}
				for (Long number : numbers) {
					System.out.println(number);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
